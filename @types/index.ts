export type Photo = {
  id: number
  name: string
}

export type Album = {
  id: number
  name: string
  photos: Photo[] | []
}
