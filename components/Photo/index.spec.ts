import { render } from '@testing-library/vue'
import AlbumDisplay from '~/components/Photo/index.vue'

const DEFAULT_COMPONENT_PROPS = {
  photos: [
    {
      id: 1,
      name: 'Photo 1',
    },
    {
      id: 2,
      name: 'Photo 2',
    },
  ],
  hasUnlinkedPhotos: false,
}

function renderView(options = {}) {
  return render(AlbumDisplay, {
    props: { ...DEFAULT_COMPONENT_PROPS },
    ...options,
  })
}

describe(`Component:`, () => {
  it('should render Albums given as Props', () => {
    const { getByText } = renderView()
    DEFAULT_COMPONENT_PROPS.photos.forEach((photo) => {
      expect(getByText(photo.name)).toBeTruthy()
    })
  })
  it('adds error class if it has unlinked Photos ', () => {
    const { container } = renderView({
      props: { ...DEFAULT_COMPONENT_PROPS, hasUnlinkedPhotos: true },
    })
    expect(container.querySelector('.error')).toBeTruthy()
  })
})
