import { render } from '@testing-library/vue'
import AlbumDisplay from '~/components/Album/index.vue'

const DEFAULT_COMPONENT_PROPS = {
  albums: [
    {
      id: 1,
      name: 'Album 1',
      photos: [],
    },
    {
      id: 2,
      name: 'Album 2',
      photos: [],
    },
  ],
}

function renderView(options = {}) {
  return render(AlbumDisplay, {
    props: { ...DEFAULT_COMPONENT_PROPS },
    ...options,
  })
}

describe(`Component:`, () => {
  it('should render Albums given as Props', () => {
    const { getByText } = renderView()
    DEFAULT_COMPONENT_PROPS.albums.forEach((album) => {
      expect(getByText(album.name)).toBeTruthy()
    })
  })
})
