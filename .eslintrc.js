module.exports = {
  extends: ['plugin:nuxt/recommended', './config/eslint-config'],

  settings: {
    // https://github.com/johvin/eslint-import-resolver-alias#readme
    'import/resolver': {
      alias: {
        map: [
          ['~', './'],
          ['@', './cypress'],
        ],
        extensions: ['.js', '.ts', '.d.ts'],
      },
    },
  },
  rules: {
    'max-nested-callbacks': ['error', { max: 3 }],
    'max-statements': [
      'error',
      {
        max: 10,
      },
      {
        ignoreTopLevelFunctions: true,
      },
    ],
    'complexity': [
      'error',
      {
        max: 5,
      },
    ],
    'max-depth': [
      'error',
      {
        max: 2,
      },
    ],
    'max-params': [
      'error',
      {
        max: 3,
      },
    ],
    'import/no-unresolved': [
      'error',
      {
        ignore: ['.svg'],
      },
    ],
    'no-underscore-dangle': [
      'error',
      {
        // Allow underscore dangle in TypeScript files for private members
        // https://www.typescriptlang.org/docs/handbook/classes.html#accessors
        // https://stackoverflow.com/questions/40587873/naming-convention-for-class-properties-in-typescript
        allowAfterThis: true,
      },
    ],
  },
  overrides: [
    {
      files: ['./cypress/**/**.js'],
      rules: {
        'jest/valid-expect-in-promise': 'off',
        'jest/valid-expect': 'off',
        'jest/no-commented-out-tests': 'off',
        'jest/no-disabled-tests': 'off',
        'max-nested-callbacks': 'off',
        'max-statements': 'off',
        'max-lines-per-function': 'off',
        'cypress/no-unnecessary-waiting': 'off',
      },
    },
    {
      files: ['*.spec.ts', '*.spec.js', '*.config.js'],
      rules: {
        'max-lines-per-function': 'off',
        'max-statements': 'off',
        'max-nested-callbacks': 'off',
        'complexity': 'off',
      },
    },
  ],
}
