module.exports = {
  extends: [
    'plugin:vue/recommended',
    'plugin:vue-types/strongly-recommended',
    'plugin:compat/recommended',
    'prettier',
    '@vue/typescript',
  ],
  plugins: ['import', 'prettier'],
  parserOptions: {
    parser: '@typescript-eslint/parser',
  },
  rules: {
    'prettier/prettier': [
      'warn',
      {
        semi: false,
        quoteProps: 'consistent',
        trailingComma: 'all',
        singleQuote: true,
        htmlWhitespaceSensitivity: 'ignore',
      },
    ],
    'import/extensions': [
      'error',
      'always',
      {
        js: 'never',
        vue: 'never',
      },
    ],
    'import/no-extraneous-dependencies': 'off',
    'semi': ['error', 'never'],
    'no-console': ['error', { allow: ['info', 'warn', 'error'] }],
    'no-unused-vars': [
      'error',
      {
        argsIgnorePattern: '^_',
      },
    ],
    'complexity': ['error', 20],
    'max-lines-per-function': [
      'warn',
      {
        max: 100,
        skipComments: true,
        skipBlankLines: true,
      },
    ],

    'vue/component-name-in-template-casing': [
      'error',
      'PascalCase',
      {
        registeredComponentsOnly: false,
        ignores: ['i18n'],
      },
    ],
    'vue/no-deprecated-scope-attribute': 'error',
    'no-restricted-imports': [
      'error',
      {
        paths: [
          {
            name: 'date-fns',
            message:
              'Please import functions from files for smaller bundle size.',
          },
          {
            name: 'lodash',
            message:
              'Please import functions from files for smaller bundle size.',
          },
        ],
      },
    ],
    // false positives with testing-library
    'jest/expect-expect': 'off',
  },
  settings: {
    // https://github.com/johvin/eslint-import-resolver-alias#readme
    'import/resolver': {
      typescript: {},
      alias: {
        map: [
          ['~', './'],
          ['@', './src'],
        ],
        extensions: ['.js', '.ts', '.d.ts'],
      },
    },
  },
}
